// Core Imports
import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { take, exhaustMap } from 'rxjs/operators';

// Services
import { LoginService } from "src/app/login/login.service";
import { Observable } from 'rxjs';

@Injectable()

export class AuthInterceptorService implements HttpInterceptor {

    constructor(private _loginService: LoginService) { }

    intercept(
        request: HttpRequest<any>, next: HttpHandler
      ) : Observable<HttpEvent<any>> {    
          const _token = localStorage.getItem('_token');
          //console.log(_token);
          if (!_token) {
            // console.log("the Last request sending  =  req  = ", req);
            return next.handle(request);
        }   
        const modifiedReq = request.clone({
            headers: request.headers.set("Authorization", "Bearer " + _token)
        });
        // console.log("the Last request sending  =  modifiedReq  = ", modifiedReq);
        return next.handle(modifiedReq);
      }

}