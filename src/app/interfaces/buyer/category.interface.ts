export interface CategoryInterface {
    id,
    name,
    icon,
    parent_id,
    slug,
    articles,
}