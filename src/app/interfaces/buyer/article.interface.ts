export interface ArticleInterface {
    id,
    title,
    content,
    category_id,
    slug,
    belongs_to,
}