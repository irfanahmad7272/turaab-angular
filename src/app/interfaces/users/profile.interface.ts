export interface UserProfileInterface {
    id,
    name,
    email,
    country_code,
    country_code_text,
    phone_number,
    profile_image,
    role,
    member_since,
    is_buyer,
    is_2fa_verified,
    is_2fa_enabled,
    profile_publicly_visible
}