import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { NgxSpinnerService } from "ngx-spinner";

import { environment } from './../../environments/environment';

let BACKEND_URL = '';
let API_ROOT_URL = '';

if(environment.production) {
    BACKEND_URL = "https://api.campfreelancer.com/"; // production
    API_ROOT_URL = 'https://api.campfreelancer.com/api/'; // production
}
else {
    BACKEND_URL = "http://localhost:8088/freelance-portal/public/"; // local
    API_ROOT_URL = 'http://localhost:8088/freelance-portal/public/api/'; // local
}

@Injectable({
    providedIn: 'root'
})

export class SystemService {
    
    // Backend & Api Root Url
    private _backendURL: string = BACKEND_URL; // this will automatically change depending on environment mode
    private _apiRootURL: string = API_ROOT_URL; // this will automatically change depending on environment mode

    private _sociallinks: SocialLoginURLs = {
        facebook: this._backendURL + 'login/facebook',
        google: this._backendURL + 'login/google',
        linkedin: this._backendURL + 'login/linkedin',
        github: this._backendURL + 'login/github'
    }

    private _alertTimeOut = 5000;

    constructor(private _spinner: NgxSpinnerService) { }

    loadingPageDataTrue() {
        this._spinner.show();
    }

    loadingPageDataFalse() {
        this._spinner.hide();
    }

    getApiRootURL() {
        return this._apiRootURL;
    }

    // get Social Login Links
    getSocialLinks() {
        return this._sociallinks;
    }

    // alert box timeout
    getAlertTimeOut() {
        return this._alertTimeOut;
    }
}

export interface SocialLoginURLs {
    facebook,
    google,
    linkedin,
    github
}