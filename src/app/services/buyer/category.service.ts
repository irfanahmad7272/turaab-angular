import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

import { SystemService } from "../system.service";

import { CategoryInterface } from '../../interfaces/buyer/category.interface';
import { Category } from 'src/app/models/buyer/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  // Category Component Variables
  private _categoryHomeData = new BehaviorSubject<CategoryInterface[]>(null);
  private _categoryAllData = new BehaviorSubject<CategoryInterface[]>(null);
  private _categorySingleData = new BehaviorSubject<CategoryInterface>(null);

  constructor(private _systemService: SystemService, private _http: HttpClient) { }

  //  Functions
  getBuyerHomeCategoryDataFromServer() {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'home/categories/buyer'
    );
  }

  getBuyerHomeCategoryData() {
    return this._categoryHomeData;
  }

  setBuyerHomeCategoryData(data: CategoryInterface[]) {
      this._categoryHomeData.next(data);
  }

  getBuyerCategoryByIdDataFromServer(categoryId: number) {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'category/' + categoryId + '/buyer'
    );
  }

  getBuyerCategoryByIdData(categoryId: number) {
    return this._categorySingleData;
  }

  setBuyerCategoryByIdData(data: CategoryInterface) {
      this._categorySingleData.next(data);
  }

  getBuyerAllCategoryDataFromServer() {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'all/categories/buyer'
    );
  }

  getBuyerAllCategoryData() {
    return this._categoryAllData;
  }

  setBuyerAllCategoryData(data: CategoryInterface[]) {
      this._categoryAllData.next(data);
  }
}
