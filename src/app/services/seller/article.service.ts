import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

import { SystemService } from "../system.service";

import { ArticleInterface } from '../../interfaces/seller/article.interface';
import { Article } from 'src/app/models/seller/article.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  // Seller Article Component Variables
  private _articleData = new BehaviorSubject<ArticleInterface>(null);

  constructor(private _systemService: SystemService, private _http: HttpClient) { }

  // Seller Article Functions
  getArticleDataByIdFromServer(articleId: number) {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'article/' + articleId + '/seller'
    );
  }

  getArticleDataById(articleId: number) {
      return this._articleData;
  }

  setArticleDataById(data: ArticleInterface) {
      this._articleData.next(data);
  }
}
