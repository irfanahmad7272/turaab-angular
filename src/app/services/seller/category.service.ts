import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

import { SystemService } from "../system.service";

import { CategoryInterface } from '../../interfaces/seller/category.interface';
import { Category } from 'src/app/models/seller/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  // Category Component Variables
  private _categoryHomeData = new BehaviorSubject<CategoryInterface[]>(null);
  private _categoryAllData = new BehaviorSubject<CategoryInterface[]>(null);
  private _categorySingleData = new BehaviorSubject<CategoryInterface>(null);

  constructor(private _systemService: SystemService, private _http: HttpClient) { }

  getSellerHomeCategoryDataFromServer() {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'home/categories/seller'
    );
  }

  getSellerHomeCategoryData() {
    return this._categoryHomeData;
  }

  setSellerHomeCategoryData(data: CategoryInterface[]) {
      this._categoryHomeData.next(data);
  }

  getSellerCategoryByIdDataFromServer(categoryId: number) {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'category/' + categoryId + '/seller'
    );
  }

  getSellerCategoryByIdData(categoryId: number) {
    return this._categorySingleData;
  }

  setSellerCategoryByIdData(data: CategoryInterface) {
      this._categorySingleData.next(data);
  }

  getSellerAllCategoryDataFromServer() {
    return this._http.get<any>(
        this._systemService.getApiRootURL() + 'all/categories/seller'
    );
  }

  getSellerAllCategoryData() {
      return this._categoryAllData;
  }

  setSellerAllCategoryData(data: CategoryInterface[]) {
      this._categoryAllData.next(data);
  }
}
