import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

import { SystemService } from "../system.service";

import { UserProfileInterface } from '../../interfaces/users/profile.interface';
import { User } from 'src/app/login/login.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    // User Profile Component Variables
    private _userProfileData = new BehaviorSubject<UserProfileInterface>(null);

    constructor(private _systemService: SystemService, private _http: HttpClient) { }

    // User Profile Functions
    getProfileDataFromServer() {
        return this._http.get<any>(
            this._systemService.getApiRootURL() + 'user/profile'
        );
    }

    getProfileData() {
        return this._userProfileData;
    }

    setProfileData(data: UserProfileInterface) {
        this._userProfileData.next(data);
    }

    updateProfileDataInServer(data) {
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'user/profile/update',
            data
        );
    }

    updateProfileImageInServer(data) {
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'user/profile-img/update',
            data
        );
    }

    // Change User Password
    changeUserPassword(data) {
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'user/change-password',
            data
        );
    }

    updateUserRole(_role: 'seller' | 'buyer') {
        let data = {
            role: _role
        };
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'user-role/update',
            data
        );
    }


    updateUserRoleLocally(role: 'seller' | 'buyer') {
        let isBuyer = false;
        let newRole = 'seller';
        if (role == 'seller') {
            isBuyer = false;
            newRole = 'seller';
        }
        else if (role == 'buyer') {
            isBuyer = true;
            newRole = 'buyer';
        }
        else {
            alert("Invalid User Role to Set User Role");
            return;
        }
        const userData: User = JSON.parse(localStorage.getItem('user_data'));
        const localUser = new User(
            userData.id,
            userData.name,
            userData.email,
            userData.phone_number,
            userData.profile_img,
            newRole,
            userData._token,
            new Date(userData.tokken_expire_time),
            userData.country_code,
            isBuyer,
            userData.is_2fa_enabled,
            userData.is_2fa_verified,
            userData.profile_publicly_visible
        );
        localStorage.setItem('user_data', JSON.stringify(localUser));
        window.location.reload();
    }

    // Delete User Account
    deleteUserAccount() {
        return this._http.delete<any>(
            this._systemService.getApiRootURL() + 'user'
        );
    }
}