import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { CategoryService as BuyerCategoryService } from 'src/app/services/buyer/category.service';

//interfaces
import { CategoryInterface as BuyerCategoryInterface } from 'src/app/interfaces/buyer/category.interface';


@Component({
  selector: 'app-buyer',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.css']
})
export class BuyerComponent implements OnInit {
  _buyerCategoryData: BuyerCategoryInterface[] = [];

  _buyerCategoryData_Sub: Subscription;
  _buyerCategoryData_Server_Sub: Subscription;
  _errors: any;

  constructor(private router: Router, private _systemService: SystemService, private _buyerCategoryService: BuyerCategoryService) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });
    this.getBuyerCategoryData();
  }

  getBuyerAllCategoryDataFromServer() {
    this._systemService.loadingPageDataTrue();

    this._buyerCategoryData_Server_Sub = this._buyerCategoryService.getBuyerAllCategoryDataFromServer().subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("SupportComponent == getBuyerAllCategoryDataFromServer == response = ", res);
        this._buyerCategoryService.setBuyerAllCategoryData(res.data);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("SupportComponent == getBuyerAllCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getBuyerCategoryData() {
    this._buyerCategoryService.getBuyerAllCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("SupportComponent == getBuyerCategoryData == response was null fetching data from server");
          this.getBuyerAllCategoryDataFromServer();
        } else {
          // console.log("SupportComponent == getBuyerCategoryData == response (was not null) = ", res);
          this._buyerCategoryData = res;
        }
      },
      err => {
        console.log("SupportComponent == getBuyerAllCategoryData == error = ", err);
      }
    );
  }


  ngOnDestroy() {
    if (this._buyerCategoryData_Sub) {
      this._buyerCategoryData_Sub.unsubscribe();
    }
    if (this._buyerCategoryData_Sub) {
      this._buyerCategoryData_Sub.unsubscribe();
    }
  }

}
