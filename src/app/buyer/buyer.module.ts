import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyerRoutingModule } from './buyer-routing.module';
import { BuyerComponent } from './buyer.component';
import { LayoutsModule } from '../layouts/layouts.module';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [BuyerComponent],
  imports: [
    CommonModule,
    BuyerRoutingModule,
    LayoutsModule,
    RouterModule,
    NgxSpinnerModule
  ]
})
export class BuyerModule { }
