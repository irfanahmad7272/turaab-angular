import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedComponent } from './shared/shared.component';

const routes: Routes = [
  {
    path: '',
    component: SharedComponent,
    children: [
      {
        path: '',
        loadChildren: './support/support.module#SupportModule',
        pathMatch: 'full'
      },
      {
        path: 'buyer',
        loadChildren: './buyer/buyer.module#BuyerModule'
      },
      {
        path: 'seller',
        loadChildren: './seller/seller.module#SellerModule'
      },
      {
        path: 'category/:id/:type',
        loadChildren: './category/category.module#CategoryModule'
      },
      {
        path: 'article/:id/:type',
        loadChildren: './article/article.module#ArticleModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
