import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd} from '@angular/router';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { CategoryService as BuyerCategoryService } from 'src/app/services/buyer/category.service';
import { CategoryService as SellerCategoryService } from 'src/app/services/seller/category.service';

//interfaces
import { CategoryInterface as BuyerCategoryInterface } from 'src/app/interfaces/buyer/category.interface';
import { CategoryInterface as SellerCategoryInterface } from 'src/app/interfaces/seller/category.interface';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  _categoryId:number = 0;
  _buyerCategoryData: BuyerCategoryInterface[] = [];
  _sellerCategoryData: SellerCategoryInterface[] = [];
  _buyerSingleCategoryData: BuyerCategoryInterface = null;
  _sellerSingleCategoryData: SellerCategoryInterface = null;

  _buyerCategoryData_Sub: Subscription;
  _buyerCategoryData_Server_Sub: Subscription;
  _sellerCategoryData_Sub: Subscription;
  _sellerCategoryData_Server_Sub: Subscription;

  _buyerSingleCategoryData_Sub: Subscription;
  _buyerSingleCategoryData_Server_Sub: Subscription;
  _sellerSingleCategoryData_Sub: Subscription;
  _sellerSingleCategoryData_Server_Sub: Subscription;

  _errors: any;
  constructor(
    private router: Router,
    private _systemService: SystemService,
    private _buyerCategoryService: BuyerCategoryService,
    private _sellerCategoryService: SellerCategoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });

    this.route.params.subscribe(params => {
      this._categoryId = params['id'];
      console.log(this._categoryId);
      
      if ( params['type'] == 'buyer' ) {
        this.getBuyerCategoryData();
        this.getBuyerCategoryByIdDataFromServer(this._categoryId);
      } else {
        this.getSellerCategoryData();
        this.getSellerCategoryByIdDataFromServer(this._categoryId);
      }
    });
  }

  getBuyerAllCategoryDataFromServer() {
    this._systemService.loadingPageDataTrue();

    this._buyerCategoryData_Server_Sub = this._buyerCategoryService.getBuyerAllCategoryDataFromServer().subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("CategoryComponent == getBuyerAllCategoryDataFromServer == response = ", res);
        this._buyerCategoryService.setBuyerAllCategoryData(res.data);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("CategoryComponent == getBuyerAllCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getSellerAllCategoryDataFromServer() {
    this._systemService.loadingPageDataTrue();

    this._sellerCategoryData_Server_Sub = this._sellerCategoryService.getSellerAllCategoryDataFromServer().subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("CategoryComponent == getSellerAllCategoryDataFromServer == response = ", res);
        this._sellerCategoryService.setSellerAllCategoryData(res.data);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("CategoryComponent == getSellerAllCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getBuyerCategoryByIdDataFromServer(categoryId) {
    this._systemService.loadingPageDataTrue();
    this._categoryId = categoryId;

    this._buyerSingleCategoryData_Server_Sub = this._buyerCategoryService.getBuyerCategoryByIdDataFromServer(categoryId).subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("CategoryComponent == getBuyerCategoryByIdDataFromServer == response = ", res);
        this._buyerCategoryService.setBuyerCategoryByIdData(res.data);
        this.getBuyerCategoryById(categoryId);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("CategoryComponent == getBuyerCategoryByIdDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getSellerCategoryByIdDataFromServer(categoryId) {
    this._systemService.loadingPageDataTrue();
    this._categoryId = categoryId;

    this._sellerSingleCategoryData_Server_Sub = this._sellerCategoryService.getSellerCategoryByIdDataFromServer(categoryId).subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("CategoryComponent == getSellerCategoryByIdDataFromServer == response = ", res);
        this._sellerCategoryService.setSellerCategoryByIdData(res.data);
        this.getSellerCategoryById(categoryId);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("CategoryComponent == getSellerCategoryByIdDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getBuyerCategoryData() {
    this._buyerCategoryService.getBuyerAllCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("CategoryComponent == getBuyerCategoryData == response was null fetching data from server");
          this.getBuyerAllCategoryDataFromServer();
        } else {
          // console.log("CategoryComponent == getBuyerCategoryData == response (was not null) = ", res);
          this._buyerCategoryData = res;
        }
      },
      err => {
        console.log("SupportComponent == getBuyerAllCategoryData == error = ", err);
      }
    );
  }

  getSellerCategoryData() {
    this._sellerCategoryService.getSellerAllCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("CategoryComponent == getSellerCategoryData == response was null fetching data from server");
          this.getSellerAllCategoryDataFromServer();
        } else {
          // console.log("CategoryComponent == getSellerCategoryData == response (was not null) = ", res);
          this._sellerCategoryData = res;
        }
      },
      err => {
        console.log("CategoryComponent == getSellerAllCategoryData == error = ", err);
      }
    );
  }

  getBuyerCategoryById(categoryId) {

    this._buyerCategoryService.getBuyerCategoryByIdData(categoryId).subscribe(
      res => {
        // console.log("CategoryComponent == getBuyerCategoryById == response (was not null) = ", res);
        this._buyerSingleCategoryData = res;
      },
      err => {
        console.log("SupportComponent == getBuyerCategoryById == error = ", err);
      }
    );
  }

  getSellerCategoryById(categoryId) {

    this._sellerCategoryService.getSellerCategoryByIdData(categoryId).subscribe(
      res => {
        // console.log("CategoryComponent == getSellerCategoryById == response (was not null) = ", res);
        this._sellerSingleCategoryData = res;
      },
      err => {
        console.log("SupportComponent == getSellerCategoryById == error = ", err);
      }
    );
  }
 

  ngOnDestroy() {

    if (this._buyerCategoryData_Sub) {
      this._buyerCategoryData_Sub.unsubscribe();
    }

    if (this._buyerCategoryData_Server_Sub) {
      this._buyerCategoryData_Server_Sub.unsubscribe();
    }

    if (this._sellerCategoryData_Sub) {
      this._sellerCategoryData_Sub.unsubscribe();
    }

    if (this._sellerCategoryData_Server_Sub) {
      this._sellerCategoryData_Server_Sub.unsubscribe();
    }

    if (this._sellerSingleCategoryData_Sub) {
      this._sellerSingleCategoryData_Sub.unsubscribe();
    }

    if (this._sellerSingleCategoryData_Server_Sub) {
      this._sellerSingleCategoryData_Server_Sub.unsubscribe();
    }

    if (this._buyerSingleCategoryData_Sub) {
      this._buyerSingleCategoryData_Sub.unsubscribe();
    }

    if (this._buyerSingleCategoryData_Server_Sub) {
      this._buyerSingleCategoryData_Server_Sub.unsubscribe();
    }
  }
}
