import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";

import { SupportRoutingModule } from './support-routing.module';
import { SupportComponent } from './support.component';
import { LayoutsModule } from '../layouts/layouts.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [SupportComponent],
  imports: [
    CommonModule,
    SupportRoutingModule,
    LayoutsModule,
    RouterModule,
    NgxSpinnerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SupportModule { }
