import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';


@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {
  _loadingStatus: boolean = false;
  _loadingStatus_Sub: Subscription;

  constructor(private _systemService: SystemService) { }

  ngOnInit() {
    this._systemService.loadingPageDataTrue();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this._systemService.loadingPageDataFalse();
    }, 3000);
  }

}
