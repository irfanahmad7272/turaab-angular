export class Article {
    constructor(
        public id,
        public title,
        public content,
        public slug,
        public category_id,
        public belongs_to
    ) {}
}