export class Category {
    constructor(
        public id,
        public name,
        public icon,
        public parent_id,
        public slug
    ) {}
}