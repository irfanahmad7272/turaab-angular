import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SellerComponent} from './seller.component';

const routes: Routes = [
  {
    path: '',
    component: SellerComponent,
    data: {
      title: 'Buyer',
      icon: 'icon-layout-sidebar-left',
      caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerRoutingModule { }
