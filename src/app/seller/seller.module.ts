import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SellerRoutingModule } from './seller-routing.module';
import { SellerComponent } from './seller.component';
import { LayoutsModule } from '../layouts/layouts.module';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [SellerComponent],
  imports: [
    CommonModule,
    SellerRoutingModule,
    LayoutsModule,
    RouterModule,
    NgxSpinnerModule
  ]
})
export class SellerModule { }
