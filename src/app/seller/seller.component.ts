import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { CategoryService as SellerCategoryService } from 'src/app/services/seller/category.service';

//interfaces
import { CategoryInterface as SellerCategoryInterface } from 'src/app/interfaces/seller/category.interface';


@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.css']
})
export class SellerComponent implements OnInit, OnDestroy {

  _sellerCategoryData: SellerCategoryInterface[] = [];

  _sellerCategoryData_Sub: Subscription;
  _sellerCategoryData_Server_Sub: Subscription;
  _errors: any;

  constructor(private router: Router, private _systemService: SystemService, private _sellerCategoryService: SellerCategoryService) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });
    this.getSellerCategoryData();
  }

  getSellerAllCategoryDataFromServer() {
    this._systemService.loadingPageDataTrue();

    this._sellerCategoryData_Server_Sub = this._sellerCategoryService.getSellerAllCategoryDataFromServer().subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("SupportComponent == getSellerAllCategoryDataFromServer == response = ", res);
        this._sellerCategoryService.setSellerAllCategoryData(res.data);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("SupportComponent == getSellerAllCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getSellerCategoryData() {
    this._sellerCategoryService.getSellerAllCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("SupportComponent == getSellerCategoryData == response was null fetching data from server");
          this.getSellerAllCategoryDataFromServer();
        } else {
          // console.log("SupportComponent == getSellerCategoryData == response (was not null) = ", res);
          this._sellerCategoryData = res;
        }
      },
      err => {
        console.log("SupportComponent == getSellerAllCategoryData == error = ", err);
      }
    );
  }


  ngOnDestroy() {
    if (this._sellerCategoryData_Sub) {
      this._sellerCategoryData_Sub.unsubscribe();
    }
    if (this._sellerCategoryData_Sub) {
      this._sellerCategoryData_Sub.unsubscribe();
    }
  }

}
