// Core Imports
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// Services
import { LoginService } from 'src/app/login/login.service';
import { UserService } from 'src/app/services/user/user.service';
import { SystemService } from 'src/app/services/system.service';

// Interfaces
import { UserProfileInterface } from 'src/app/interfaces/users/profile.interface';


@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit, OnDestroy {
  _userProfileData: UserProfileInterface = null;

  // Subscriptions
  _userProfileData_Sub: Subscription;
  _userProfileData_Server_Sub: Subscription;
  _updateUserProfileData_Server_Sub: Subscription;
  _updateUserRole_Sub: Subscription;

  _errors: any;

  constructor(private _systemService: SystemService, private _userServce: UserService, private _loginService: LoginService) { }

  get profileImgLink() {
    if (this._userProfileData.profile_image) {
      return this._userProfileData.profile_image;
    } else {
      return 'assets/img/placeholder.jpg';
    }
  }

  ngOnInit() {
    this.getUserProfileData();
  }

  getUserProfileDataFromServer() {
    this._userProfileData_Server_Sub = this._userServce.getProfileDataFromServer().subscribe(
      res => {
        // console.log("UserAccountComponent == getUserProfileDataFromServer == response = ", res);
        this._userServce.setProfileData(res.data);
      },
      err => {
        console.log("UserAccountComponent == getUserProfileDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getUserProfileData() {
    this._userServce.getProfileData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("UserAccountComponent == getUserProfileData == response was null fetching data from server");
          this.getUserProfileDataFromServer();
        } else {
          // console.log("UserAccountComponent == getUserProfileData == response (was not null) = ", res);
          this._userProfileData = res;
        }
      },
      err => {
        console.log("UserAccountComponent == getUserProfileData == error = ", err);
      }
    );
  }

  logout() {
    this._loginService.logout();
  }

  updateUserRole(role: 'seller'|'buyer') {
    this._systemService.loadingPageDataTrue();
    this._updateUserRole_Sub = this._userServce.updateUserRole(role).subscribe(
      res => {
        this._userServce.updateUserRoleLocally(role);
      },
      err => {
        console.log("UserAccountComponent == getUserProfileDataFromServer == error = ", err);
        this._errors = err.error.errors;
        this._systemService.loadingPageDataFalse();
      }
    );
  }

  ngOnDestroy() {
    if (this._userProfileData_Sub) {
      this._userProfileData_Sub.unsubscribe();
    }
    if (this._userProfileData_Server_Sub) {
      this._userProfileData_Server_Sub.unsubscribe();
    }
    if (this._updateUserProfileData_Server_Sub) {
      this._updateUserProfileData_Server_Sub.unsubscribe();
    }
  }

}
