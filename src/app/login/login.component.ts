import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { LoginService } from './login.service'
// Interface
import { SocialLoginURLs } from 'src/app/services/system.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  _loadingStatus: boolean = false;
  _loadingStatus_Sub: Subscription;
  user = {
    email: '',
    password: ''
  };
  passwordInputFieldType: 'password' | 'text' = 'password';

  _formSubmited: boolean = false;
  _formSubmited_Sub: Subscription;

  _sociallinks: SocialLoginURLs;

  _errorOccured: boolean = false;
  _errorMessage: string = 'Error Occured!';
  _errors: any;

  constructor(private _systemService: SystemService, private _loginService: LoginService) { }

  ngOnInit(): void {
    this._systemService.loadingPageDataTrue();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this._systemService.loadingPageDataFalse();
    }, 2000);
    
    // social login links
    this.getSocialLoginURLs();
  }

  changePasswordFieldType() {
    if (this.passwordInputFieldType == 'password') {
      this.passwordInputFieldType = 'text';
    }
    else {
      this.passwordInputFieldType = 'password';
    }
  }

  onSubmit() {
    this._formSubmited = true;
    this._formSubmited_Sub = this._loginService.signIn(this.user).subscribe(
      res => {
        this._loginService.setCurrentUserData(res.data);
        window.location.reload();
      },
      err => {
        console.log("SignInComponent == onSubmit == error = ", err);
        this._errorOccured = true;
        this._errorMessage = err.error.message ? err.error.message : 'Error Occured!';
        this._errors = err.error.errors;
        this._formSubmited = false;
        // console.log('_errorOccured = ', this._errorOccured, '_errorMessage = ', this._errorMessage);
        this._systemService.loadingPageDataFalse();
      }
    );
  }

  resetForm() {
    this._formSubmited = false;
    this.user = {
      email: '',
      password: ''
    };
  }

  getSocialLoginURLs() {
    this._sociallinks = this._systemService.getSocialLinks();
  }
  
  ngOnDestroy(): void {
    if (this._loadingStatus_Sub) {
      this._loadingStatus_Sub.unsubscribe();
    }
    if (this._formSubmited_Sub) {
      this._formSubmited_Sub.unsubscribe();
    }
  }

}
