import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { BehaviorSubject, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";

import { SystemService } from '../services/system.service';

import { User } from './login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _user = new BehaviorSubject<User>(null);
    private _tokkenExpirationTime: any = null;

    private _userRole = new BehaviorSubject<'admin' | 'buyer' | 'seller' | 'user'>(null);

    constructor(
        private _router: Router,
        private _http: HttpClient,
        private _systemService: SystemService
    ) { }

    getCurrentUserData() {
        return this._user;
    }

    setCurrentUserData(data) {
        localStorage.setItem('_token', data.token);
        this._user.next(data);
    }

    getUserRole() {
        return this._userRole;
    }

    setUserRole(role: 'admin' | 'buyer' | 'seller' | 'user') {
        this._userRole.next(role);
    }

    signIn(data) {
        //check user to sign in
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'login',
            data
        )
            .pipe(
                catchError(this.errorHandler),
                tap(
                    res => {
                        this.authManager(res.data);
                    }
                )
            );
    }

    signUp(data) {
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'register',
            data
        )
            .pipe(
                catchError(this.errorHandler),
                tap(
                    res => {
                        this.authManager(res.data);
                    }
                )
            );
    }

    forgotPassword() {
        this._router.navigate(['/home']);
    }

    logout() {
        // this.logoutUserFormApp();
        this._systemService.loadingPageDataTrue();
        const data = "ok";
        this._http.post<any>(
            this._systemService.getApiRootURL() + 'logout',
            data
        ).subscribe(
            res => {
                this.logoutUserFormApp();
                window.location.reload();
                // console.log('Logout Request API Done, Response = ', res);
            },
            err => {
                this.logoutUserFormApp();
                window.location.reload();
                // console.log('Error While Logout Request API, Error = ', err);
            }
        );
    }

    logoutUserFormApp() {
        localStorage.removeItem('user_data');
        this._user.next(null);
        if (this._tokkenExpirationTime) {
            clearTimeout(this._tokkenExpirationTime);
        }
        this._tokkenExpirationTime = null;
        this._router.navigate(['/sign-in']);
    }

    autoLogin() {
        if (localStorage.getItem('user_data')) {
            const userData: User = JSON.parse(localStorage.getItem('user_data'));
            const localUser = new User(
                userData.id,
                userData.name,
                userData.email,
                userData.phone_number,
                userData.profile_img,
                userData.role,
                userData._token,
                new Date(userData.tokken_expire_time),
                userData.country_code,
                userData.is_buyer,
                userData.is_2fa_enabled,
                userData.is_2fa_verified,
                userData.profile_publicly_visible
            );
            this._user.next(localUser);
            this._userRole.next(localUser.role);
            this.checkLoginStatus();
        } else {
            console.log("User Data not Found");
            return;
        }
    }

    checkLoginStatus() {
        this._http.get(
            this._systemService.getApiRootURL() + 'check-login-status'
        ).subscribe(
            res => {

            },
            err => {
                if (err.status == 401) {
                    this.logout();
                }
            }
        );
    }

    private errorHandler(errorRes: HttpErrorResponse) {
        let errorMessage = "Error Occured"
        console.log("check this ok", errorRes.status);
        if (errorRes.status == 401) {
            this.logout();
        }
        switch (errorRes.message) {
            case "invalid Data":
                errorMessage = "Invalid Data Entered";
                break;
        }
        return throwError(errorRes);
    }

    authManager(response) {
        // console.log('authManager  ==  response', response);
        const expireDate = new Date((new Date().getTime() + 10000) * 1000);
        const newUser = new User(
            response.id,
            response.name,
            response.email,
            response.phone_number,
            response.profile_img,
            response.role,
            response.tokken,
            expireDate,
            response.country_code,
            response.is_buyer,
            response.is_2fa_enabled,
            response.is_2fa_verified,
            response.profile_publicly_visible
        );
        this._user.next(newUser);
        this._userRole.next(newUser.role);
        localStorage.setItem('user_data', JSON.stringify(newUser));
    }

    SocialLoginCallBack(data) {
        return this._http.get<any>(
            this._systemService.getApiRootURL() + 'user/social-login',
            {
                headers: {
                    'Authorization': "Bearer " + data.token
                }
            }
        ).pipe(
            catchError(this.errorHandler),
            tap(
                res => {
                    this.authManager(res.data);
                }
            )
        );
    }

    StartTwoFactorAuth() {
        return this._http.get<any>(
            this._systemService.getApiRootURL() + 'verify-account'
        );
    }

    verifyUserAccount(data) {
        console.log("AuthService == verifyUserAccount == data = ", data);
        return this._http.post<any>(
            this._systemService.getApiRootURL() + 'verify-account',
            data
        );
    }

    resendVerificationCode() {
        return this._http.get<any>(
            this._systemService.getApiRootURL() + 'resend-verification-code'
        );
    }
}
