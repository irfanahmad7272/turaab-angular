export class User {
    constructor(
        public id,
        public name,
        public email,
        public phone_number,
        public profile_img,
        public role,
        public _token,
        public tokken_expire_time,
        public country_code?,
        public is_buyer?,
        public is_2fa_enabled?,
        public is_2fa_verified?,
        public profile_publicly_visible?
    ) {}

        // get user tokken
        get token() {
            // if (!this.tokken_expire_time || new Date() > this.tokken_expire_time) {
            //     return null;
            // }
            return this._token;
        }
}