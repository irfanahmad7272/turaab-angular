import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd} from '@angular/router';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { ArticleService as BuyerArticleService } from 'src/app/services/buyer/article.service';
import { ArticleService as SellerArticleService } from 'src/app/services/seller/article.service';

//interfaces
import { ArticleInterface as BuyerArticleInterface } from 'src/app/interfaces/buyer/article.interface';
import { ArticleInterface as SellerArticleInterface } from 'src/app/interfaces/seller/article.interface';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  _articleId:number = 0;
  _buyerArticleData: BuyerArticleInterface = null;
  _sellerArticleData: SellerArticleInterface = null;

  _buyerArticleData_Sub: Subscription;
  _buyerArticleData_Server_Sub: Subscription;
  _sellerArticleData_Sub: Subscription;
  _sellerArticleData_Server_Sub: Subscription;

  _errors: any;

  constructor(
    private router: Router,
    private _systemService: SystemService,
    private _buyerArticleService: BuyerArticleService,
    private _sellerArticleService: SellerArticleService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
    });

    this.route.params.subscribe(params => {
      this._articleId = params['id'];
      
      if ( params['type'] == 'buyer' ) {
        this.getBuyerArticleByIdDataFromServer(this._articleId);
      } else {
        this.getSellerArticleByIdDataFromServer(this._articleId);
      }
    });

    this.sideNavJquery();
  }


  getBuyerArticleByIdDataFromServer(articleId) {
    this._systemService.loadingPageDataTrue();
    this._articleId = articleId;

    this._buyerArticleData_Server_Sub = this._buyerArticleService.getArticleDataByIdFromServer(articleId).subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("ArticleComponent == getArticleDataByIdFromServer == response = ", res);
        this._buyerArticleService.setArticleDataById(res.data);
        this.getBuyerArticleById(articleId);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("ArticleComponent == getArticleDataByIdFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getBuyerArticleById(articleId) {

    this._buyerArticleService.getArticleDataById(articleId).subscribe(
      res => {
        // console.log("ArticleComponent == getBuyerArticleById == response (was not null) = ", res);
        this._buyerArticleData = res;
      },
      err => {
        console.log("ArticleComponent == getBuyerArticleById == error = ", err);
      }
    );
  }



  getSellerArticleByIdDataFromServer(articleId) {
    this._systemService.loadingPageDataTrue();
    this._articleId = articleId;

    this._sellerArticleData_Server_Sub = this._sellerArticleService.getArticleDataByIdFromServer(articleId).subscribe(
      res => {
        this._systemService.loadingPageDataFalse();
        // console.log("ArticleComponent == getSellerArticleByIdDataFromServer == response = ", res);
        this._sellerArticleService.setArticleDataById(res.data);
        this.getSellerArticleById(articleId);
      },
      err => {
        this._systemService.loadingPageDataFalse();
        //console.log("ArticleComponent == getSellerArticleByIdDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getSellerArticleById(articleId) {

    this._sellerArticleService.getArticleDataById(articleId).subscribe(
      res => {
        // console.log("ArticleComponent == getSellerArticleById == response (was not null) = ", res);
        this._sellerArticleData = res;
      },
      err => {
        console.log("ArticleComponent == getSellerArticleById == error = ", err);
      }
    );
  }

  sideNavJquery() {
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var dropdownContent = this.nextElementSibling;
      if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
      } else {
      dropdownContent.style.display = "block";
      }
      });
    }
  }

}
