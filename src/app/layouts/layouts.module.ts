import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HelpTopicsComponent } from './help-topics/help-topics.component';
import { InfoTabsComponent } from './info-tabs/info-tabs.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [HeaderComponent, NavbarComponent, HelpTopicsComponent, InfoTabsComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    NavbarComponent,
    HelpTopicsComponent,
    InfoTabsComponent,
    FooterComponent
  ]
})
export class LayoutsModule { }
