import { Component, OnInit } from '@angular/core';
import { User } from '../../login/login.model';
import { BehaviorSubject, throwError } from "rxjs";


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private _user = new BehaviorSubject<User>(null);
  constructor() { }

  ngOnInit() {
    console.log('navbar');
    console.log(this._user);
  }

}
