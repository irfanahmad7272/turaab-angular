import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// Services
import { SystemService } from 'src/app/services/system.service';
import { CategoryService as BuyerCategoryService } from 'src/app/services/buyer/category.service';
import { CategoryService as SellerCategoryService } from 'src/app/services/seller/category.service';

//interfaces
import { CategoryInterface as BuyerCategoryInterface } from 'src/app/interfaces/buyer/category.interface';
import { CategoryInterface as SellerCategoryInterface } from 'src/app/interfaces/seller/category.interface';


@Component({
  selector: 'app-info-tabs',
  templateUrl: './info-tabs.component.html',
  styleUrls: ['./info-tabs.component.css']
})
export class InfoTabsComponent implements OnInit, OnDestroy {

  _buyerCategoryData: BuyerCategoryInterface[] = [];
  _sellerCategoryData: SellerCategoryInterface[] = [];

  _sellerCategoryData_Sub: Subscription;
  _sellerCategoryData_Server_Sub: Subscription;
  _buyerCategoryData_Sub: Subscription;
  _buyerCategoryData_Server_Sub: Subscription;
  _errors: any;

  constructor(private _systemService: SystemService, private _buyerCategoryService: BuyerCategoryService, private _sellerCategoryService: SellerCategoryService) { }

  ngOnInit() {
    this.getSellerHomeCategoryData();
    this.getBuyerHomeCategoryData();
  }

  getBuyerHomeCategoryDataFromServer() {
    this._buyerCategoryData_Server_Sub = this._buyerCategoryService.getBuyerHomeCategoryDataFromServer().subscribe(
      res => {
        // console.log("SupportComponent == getBuyerHomeCategoryDataFromServer == response = ", res);
        this._buyerCategoryService.setBuyerHomeCategoryData(res.data);
      },
      err => {
        //console.log("SupportComponent == getBuyerHomeCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getBuyerHomeCategoryData() {
    this._buyerCategoryService.getBuyerHomeCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("SupportComponent == getBuyerCategoryData == response was null fetching data from server");
          this.getBuyerHomeCategoryDataFromServer();
        } else {
          // console.log("SupportComponent == getBuyerCategoryData == response (was not null) = ", res);
          this._buyerCategoryData = res;
        }
      },
      err => {
        console.log("SupportComponent == getBuyerCategoryData == error = ", err);
      }
    );
  }


  getSellerHomeCategoryDataFromServer() {
    this._sellerCategoryData_Server_Sub = this._sellerCategoryService.getSellerHomeCategoryDataFromServer().subscribe(
      res => {
        // console.log("SupportComponent == getSellerHomeCategoryDataFromServer == response = ", res);
        this._sellerCategoryService.setSellerHomeCategoryData(res.data);
      },
      err => {
        //console.log("SupportComponent == getSellerHomeCategoryDataFromServer == error = ", err);
        this._errors = err.error.errors;
      }
    );
  }

  getSellerHomeCategoryData() {
    this._sellerCategoryService.getSellerHomeCategoryData().subscribe(
      res => {
        if (!(res != null)) {
          // console.log("SupportComponent == getSellerCategoryData == response was null fetching data from server");
          this.getSellerHomeCategoryDataFromServer();
        } else {
          // console.log("SupportComponent == getSellerCategoryData == response (was not null) = ", res);
          this._sellerCategoryData = res;
        }
      },
      err => {
        console.log("SupportComponent == getSellerCategoryData == error = ", err);
      }
    );
  }

  ngOnDestroy() {
    if (this._sellerCategoryData_Sub) {
      this._sellerCategoryData_Sub.unsubscribe();
    }
    if (this._sellerCategoryData_Sub) {
      this._sellerCategoryData_Sub.unsubscribe();
    }

    if (this._buyerCategoryData_Sub) {
      this._buyerCategoryData_Sub.unsubscribe();
    }
    if (this._buyerCategoryData_Sub) {
      this._buyerCategoryData_Sub.unsubscribe();
    }
  }

}
