import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // function() {
    //   s.add(d).addClass("active"), r.addClass("dx-navbar-fullscreen-open"), r.removeClass("dx-navbar-fullscreen-closed"), r.css({
    //     "z-index": 1e3
    //   }), (0, c.bodyOverflow)(1)

      
    // }
    $('.dx-navbar-burger').on('click', function(){
      //alert("called");
      $(this).addClass('active');
      $('.dx-navbar-fullscreen').addClass('dx-navbar-fullscreen-open');
      $('.dx-navbar-fullscreen .dx-navbar-burger').addClass('active');
      $('.dx-navbar-fullscreen .dx-navbar-burger').css({
        "position": "absolute", "top": "30px", "left": "320px"
      });
      $('.dx-navbar-dropdown').addClass('collapse');
      $('.dx-navbar-fullscreen').css('z-index', 1000);
    });


    $('.dx-navbar-fullscreen .dx-navbar-burger').on('click', function(){
      //alert("called");
      $(this).removeClass('active');
      $('.dx-navbar-fullscreen').removeClass('dx-navbar-fullscreen-open');
      $('.dx-navbar-burger').removeClass('active');
      $('.dx-navbar-fullscreen .dx-navbar-burger').removeAttr('style');
      $('.dx-navbar-dropdown').removeClass('collapse');
      $('.dx-navbar-fullscreen').css('z-index', -1000);
    });


    $('.dx-drop-item a').on('click', function(event){
      event.preventDefault();
      // var element = $(this).next();
      // $(element).removeClass('collapse');
      // $(element).addClass('collapsing');

      // setTimeout(function () { 
      //   $(element).removeClass('collapsing');
      //   $(element).addClass('show');
      // }, 1000);
    });

    
  }

}
